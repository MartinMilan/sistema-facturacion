Insert INTO clientes (nombre, apellido, email, create_at, foto) values ('Martin', 'Milan', 'carlos000@correo.com', '2019-12-26', '');
Insert INTO clientes (nombre, apellido, email, create_at, foto) values ('Julio', 'Zapata', 'pepejul@correo.com', '2019-11-12', '');
Insert INTO clientes (nombre, apellido, email, create_at, foto) values ('Florencia', 'Carbonatti', 'flor.carbon@hipermail.com', '2019-12-19', '');
Insert INTO clientes (nombre, apellido, email, create_at, foto) values ('alejandra', 'Acosta', 'ale.cos@hipermail.com', '2018-8-12', '');
Insert INTO clientes (nombre, apellido, email, create_at, foto) values ('Pamela', 'Castro', 'castro.pame@correo.com', '2019-10-21', '');
Insert INTO clientes (nombre, apellido, email, create_at, foto) values ('Javier', 'Ruiz', 'javier_09@correo.com', '2019-11-12', '');
Insert INTO clientes (nombre, apellido, email, create_at, foto) values ('Odilia', 'Fernandez', 'odilia.fernandez@hipermail.com', '2019-1-18', '');
Insert INTO clientes (nombre, apellido, email, create_at, foto) values ('Zelma', 'Ledezma', 'zlemi.87@correo.com', '2019-10-2', '');
Insert INTO clientes (nombre, apellido, email, create_at, foto) values ('Quirina', 'Torres', 'quiri.torres2000@correo.com', '2019-2-13', '');
Insert INTO clientes (nombre, apellido, email, create_at, foto) values ('Isabel', 'Pérez', 'isabelita12@correo.com', '2018-6-6', '');
Insert INTO clientes (nombre, apellido, email, create_at, foto) values ('Kalena', 'Cocinero', 'kalekale_09@correo.com', '2017-9-22', '');
Insert INTO clientes (nombre, apellido, email, create_at, foto) values ('Kenaz', 'Carrizo', 'kenaz.98@correo.com', '2018-10-8', '');
Insert INTO clientes (nombre, apellido, email, create_at, foto) values ('Victoria', 'Espinoza', 'vicky.78@correo.com', '2017-5-23', '');
Insert INTO clientes (nombre, apellido, email, create_at, foto) values ('Matias', 'Abbatelli', 'mati.abba.12@correo.com', '2019-10-1', '');
Insert INTO clientes (nombre, apellido, email, create_at, foto) values ('William', 'Peñalosa', 'willis.peña93@hipermail.com', '2016-12-29', '');
Insert INTO clientes (nombre, apellido, email, create_at, foto) values ('Teresa', 'Garcia', 'teresita72@hipermail.com', '2018-4-10', '');
Insert INTO clientes (nombre, apellido, email, create_at, foto) values ('Yamila', 'Lima', 'yami.lima88@correo.com', '2018-12-31', '');
Insert INTO clientes (nombre, apellido, email, create_at, foto) values ('Zoé', 'Farías', 'zoe98@correo.com', '2019-7-7', '');
Insert INTO clientes (nombre, apellido, email, create_at, foto) values ('Xenia', 'Vega', 'vega.xenia@correo.com', '2019-2-2', '');
Insert INTO clientes (nombre, apellido, email, create_at, foto) values ('Andrea', 'Cáceres', 'andrea.89@hipermail.com', '2017-9-28', '');
Insert INTO clientes (nombre, apellido, email, create_at, foto) values ('Lázaro', 'Frankestein', 'lazaro88@correo.com', '2019-3-10', '');
Insert INTO clientes (nombre, apellido, email, create_at, foto) values ('Ianina', 'Acosta', 'ianicosta_90@correo.com', '2018-07-28', '');
Insert INTO clientes (nombre, apellido, email, create_at, foto) values ('Elisa', 'Correa', 'elisa2017@correo.com', '2018-8-4', '');
Insert INTO clientes (nombre, apellido, email, create_at, foto) values ('Ottone', 'Sislack', 'otto91@hipermail.com', '2019-4-29', '');
Insert INTO clientes (nombre, apellido, email, create_at, foto) values ('Kensel', 'Williams', 'kensel.w78@correo.com', '2017-10-17', '');
Insert INTO clientes (nombre, apellido, email, create_at, foto) values ('Alma', 'Sosa', 'alma2001@hipermail.com', '2019-4-12', '');
Insert INTO clientes (nombre, apellido, email, create_at, foto) values ('Galvin', 'Smith', 'galvin.smith90@hipermail.com', '2019-5-11', '');


/* Creamos varias enradas para la tabla productos*/

INSERT INTO productos(nombre, precio, create_at) values ("Monitor LG LED 22MK400HB", 14999, NOW());
INSERT INTO productos(nombre, precio, create_at) values ("Camara Sony DSC-WX500", 29999, NOW());
INSERT INTO productos(nombre, precio, create_at) values ("Notebook Gamer ASUS FX504GMEN476T", 89499, NOW());
INSERT INTO productos(nombre, precio, create_at) values ("Samsung Galaxy S10 G973 AZUL", 77499, NOW());
INSERT INTO productos(nombre, precio, create_at) values ("Sony PlayStation CUH2215B 1 TB", 34999, NOW());
INSERT INTO productos(nombre, precio, create_at) values ("Xbox ONE S 1TB", 29999, NOW());
INSERT INTO productos(nombre, precio, create_at) values ("Samsung Galaxy T510 101", 21899, NOW());
INSERT INTO productos(nombre, precio, create_at) values ("Samsung Smart TV 32 HD UN32J4290AG", 17499, NOW());
INSERT INTO productos(nombre, precio, create_at) values ("Logitech G433 71 BLACK", 8559, NOW());

/*Vamos a crear algunas facturas*/
INSERT INTO facturas(descripcion, observacion, cliente_id, create_at) VALUES ('Factura equipos de oficina', null, 1, NOW());
INSERT INTO factura_items(cantidad, factura_id, producto_id) VALUES (1, 1, 1);
INSERT INTO factura_items(cantidad, factura_id, producto_id) VALUES (2, 1, 4);
INSERT INTO factura_items(cantidad, factura_id, producto_id) VALUES (1, 1, 5);
INSERT INTO factura_items(cantidad, factura_id, producto_id) VALUES (1, 1, 7);

INSERT INTO facturas(descripcion, observacion, cliente_id, create_at) VALUES ('Factura equipos informaticos', null, 1, NOW());
INSERT INTO factura_items(cantidad, factura_id, producto_id) VALUES (2, 2, 3);
INSERT INTO factura_items(cantidad, factura_id, producto_id) VALUES (2, 2, 9);


/*Creamos algunos usuarios*/
INSERT INTO users (username, password, enabled) VALUES('martin', '$2a$10$uz.nKUluc4nyAFNNkjft5e49SPrEtA4xg3xtVO/UILxFVhoivIbJi', 1);
INSERT INTO users (username, password, enabled) VALUES('admin', '$2a$10$TSVPPaUgS3k9j2y0RPfMVuxlmU5XN8DcqGokOHM33oDiZV8zyhgVa', 1);

INSERT INTO authorities (user_id, authority) VALUES(1, 'ROLE_USER');
INSERT INTO authorities (user_id, authority) VALUES(2, 'ROLE_USER');
INSERT INTO authorities (user_id, authority) VALUES(2, 'ROLE_ADMIN');