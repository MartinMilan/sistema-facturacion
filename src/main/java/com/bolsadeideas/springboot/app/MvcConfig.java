package com.bolsadeideas.springboot.app;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import java.nio.file.Paths;
import java.util.Locale;

/**
 * En esta clase lo que vamos a ser es una configuración para poder subir las imagenes a un directorio externo al
 * proyecto
 *
 * Los ** son para mapear al nombre de la imagen
 */
@Configuration
public class MvcConfig implements WebMvcConfigurer {
    /*@Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        WebMvcConfigurer.super.addResourceHandlers(registry);
        String resourcePath = Paths.get("uploads").toAbsolutePath().toUri().toString(); //toUri agrega el esquema "file:/" etc
        //registry.addResourceHandler("/uploads/**").addResourceLocations("file:/C:/Temp/uploads/");
        registry.addResourceHandler("/uploads/**").addResourceLocations(resourcePath);
    }*/

    /**
     * Esta es una implementación de un controlador parametrizable. Es utilizado cuando un controlador tiene como único
     * objetivo mostrar una vista. De esta forma implementamos un ViewController. Con .addViewController agregamos la
     * url del Getmapping o RequestMapping y con setViewName el nombre de la vista que queremos cargar (.html)
     * @param registry
     */
    public void addViewControllers(ViewControllerRegistry registry){
        registry.addViewController("/error_403").setViewName("error_403");
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    /**
     * Método necesario para el multilenguage. Este método resuelve el Locale, donde se va a guardar el parámetro de
     * nuestro lenguage, en este caso en la Session
     */

    @Bean
    public LocaleResolver localeResolver(){
        SessionLocaleResolver localeResolver = new SessionLocaleResolver();
        localeResolver.setDefaultLocale(new Locale("es", "ES")); //Por defecto nuestro sitio web estará en español de España
        return localeResolver;
    }

    /**
     * Método interceptor para cambiar el idioma de nuestra página. Cuando se pase por url el parámetro lang por ejemplo
     * con el valor es_ES se va a ejecutar este interceptor y va a realizar el cambio de idioma
     */
    @Bean
    public LocaleChangeInterceptor localeChangeInterceptor(){
        LocaleChangeInterceptor localeInterceptor = new LocaleChangeInterceptor();
        localeInterceptor.setParamName("lang");
        return localeInterceptor;
    }
    /**
     * Ahora hay que registrar el interceptor creado en el método localeChangeInterceptor con el siguiente método
     */

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(localeChangeInterceptor());
    }

    /**
     * Este método retorna una instancia de Jaxb2Marshaller que es el que se encarga de serializar los objetos
     * Con JSON se puede serializarde forma transparente las colecciones pero con XML no
     * @return
     */
    @Bean
    public Jaxb2Marshaller jaxb2Marshaller(){
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setClassesToBeBound(new Class[] {com.bolsadeideas.springboot.app.view.xml.ClienteList.class}); //arreglo de clases que vamos a serializar a XML
        return marshaller;
    }
}
