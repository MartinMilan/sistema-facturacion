package com.bolsadeideas.springboot.app.view.pdf;


import com.bolsadeideas.springboot.app.models.entity.Factura;
import com.bolsadeideas.springboot.app.models.entity.ItemFactura;
import com.lowagie.text.Document;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.view.document.AbstractPdfView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.util.Locale;
import java.util.Map;

/**
 * Lo mapeamos a la misma url que el metodo ver de factura, para poder tener la opción de mos trar el pdf o el html
 */

@Component("factura/ver")
public class FacturaPdfView extends AbstractPdfView {

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private LocaleResolver localeResolver;

    @Override
    protected void buildPdfDocument(Map<String, Object> map, Document document, PdfWriter pdfWriter, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
        Factura factura = (Factura) map.get("factura");
        Locale locale = localeResolver.resolveLocale(httpServletRequest);
        //Esta representa otra forma de usar las traducciones un poco mas fácil usando la clase padre
        MessageSourceAccessor mensajes = getMessageSourceAccessor();


        PdfPTable tablaInfoCliente = new PdfPTable(1);
        tablaInfoCliente.setSpacingAfter(20);
        PdfPCell cell = null;
        cell = new PdfPCell(new Phrase(messageSource.getMessage("text.factura.ver.datos.cliente", null, locale)));
        cell.setBackgroundColor(new Color(184, 218, 255));
        cell.setPadding(8f);
        tablaInfoCliente.addCell(cell);


        tablaInfoCliente.addCell(factura.getCliente().toString());
        tablaInfoCliente.addCell(factura.getCliente().getEmail());


        PdfPTable tablaInfoFactura = new PdfPTable(1);

        cell = new PdfPCell(new Phrase(messageSource.getMessage("text.factura.ver.datos.factura", null, locale)));
        cell.setBackgroundColor(new Color(195, 230, 203));
        cell.setPadding(8f);
        tablaInfoFactura.addCell(cell);

        tablaInfoFactura.addCell(mensajes.getMessage("text.cliente.factura.folio") + ": " + factura.getId());
        tablaInfoFactura.addCell(mensajes.getMessage("text.cliente.factura.descripcion") + ": " + factura.getDescripcion());
        tablaInfoFactura.addCell(mensajes.getMessage("text.cliente.factura.fecha") + ": " + factura.getCreateAt());
        tablaInfoFactura.setSpacingAfter(20);

        document.add(tablaInfoCliente);
        document.add(tablaInfoFactura);


        PdfPTable tablaDetalleFactura = new PdfPTable(4);
        tablaDetalleFactura.setWidths(new float[] {2.5f, 1, 1, 1});
        tablaDetalleFactura.addCell(mensajes.getMessage("text.factura.form.item.nombre"));
        tablaDetalleFactura.addCell(mensajes.getMessage("text.factura.form.item.precio"));
        tablaDetalleFactura.addCell(mensajes.getMessage("text.factura.form.item.cantidad"));
        tablaDetalleFactura.addCell(mensajes.getMessage("text.factura.form.item.total"));
        for (ItemFactura item : factura.getItems()){
            tablaDetalleFactura.addCell(item.getProducto().getNombre());
            tablaDetalleFactura.addCell(item.getProducto().getPrecio().toString());
            tablaDetalleFactura.addCell(item.getCantidad().toString());
            tablaDetalleFactura.addCell(item.calcularImporte().toString());
        }
        cell = new PdfPCell(new Phrase(mensajes.getMessage("text.factura.form.total") + ": "));
        cell.setColspan(3);
        cell.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        tablaDetalleFactura.addCell(cell);
        tablaDetalleFactura.addCell(factura.getTotal().toString());

        document.add(tablaDetalleFactura);
    }
}
