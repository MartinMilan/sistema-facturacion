package com.bolsadeideas.springboot.app.view.xlsx;

import com.bolsadeideas.springboot.app.models.entity.Cliente;
import com.bolsadeideas.springboot.app.models.service.IClienteService;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.document.AbstractXlsxView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

@Component("listar.xlsx")
public class ClienteXlsxView extends AbstractXlsxView {

    @Autowired
    private IClienteService clienteService;

    @Override
    protected void buildExcelDocument(Map<String, Object> map, Workbook workbook, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
        httpServletResponse.setHeader("Content-Disposition", "attachment; filename=\"clientes.xlsx\"");
        MessageSourceAccessor mensajes = getMessageSourceAccessor();

        List<Cliente> clientes = clienteService.findAll();
        Sheet sheet = workbook.createSheet("Clientes");

        for (int i = 0; i < clientes.size(); i++) {
            Row fila = sheet.createRow(i);
            fila.createCell(0).setCellValue(clientes.get(i).getNombre());
            fila.createCell(1).setCellValue(clientes.get(i).getApellido());
            fila.createCell(2).setCellValue(clientes.get(i).getEmail());
        }



    }
}
