package com.bolsadeideas.springboot.app.Controllers;

import com.bolsadeideas.springboot.app.models.entity.Cliente;
import com.bolsadeideas.springboot.app.models.service.IClienteService;
import com.bolsadeideas.springboot.app.models.service.IUploadFileService;
import com.bolsadeideas.springboot.app.util.paginator.PageRender;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;


@Controller
@SessionAttributes("cliente")
public class ClientController {

    protected final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    private IClienteService clienteService;

    @Autowired
    private IUploadFileService uploadFileService;

    @Autowired
    private MessageSource messageSource;


    /**
     * Este método se encargará de retornar únicamente un Json
     * @ResponseBody signigica que el listado de clientes se va a almacenar en el cuerpo de la respuesta
     * @return Lista de clientes en formato json
     */
    @GetMapping(value = "/listar-rest")
    public @ResponseBody List<Cliente> listarRest(){

        return clienteService.findAll();
    }


    @RequestMapping(value = {"/listar", "/"}, method = RequestMethod.GET)
    public String listar(@RequestParam(name = "page", defaultValue = "0") int page, Model model,
                         Authentication authentication, HttpServletRequest request, Locale locale){

        if (authentication != null){
            logger.info("Hola usuario autenticado, tu username es: ".concat(authentication.getName()));
        }

        Authentication auth = SecurityContextHolder.getContext().getAuthentication(); //Equivalente al Authentication inyectado en el parámetro

        /**
         * Alternativas para verificar el rol del usuario:
         * Alternativa 1: Manualmente implementando el método hasRole()
         */
        if (hasRole("ROLE_ADMIN")){
            logger.info("Hola ".concat(auth.getName()).concat(" tienes acceso"));
        } else {
            logger.info("Hola ".concat(auth.getName()).concat(" NO tienes acceso"));
        }

        /**
         * Alaternativa 2: Esta es otra alternativa para verificar el ROL del usuario usando
         * SecurityContextHolderAwareRequestWrapper
         */
        SecurityContextHolderAwareRequestWrapper securityContext = new SecurityContextHolderAwareRequestWrapper(request, "ROLE_");
        if (securityContext.isUserInRole("ADMIN")){
            logger.info("Hola ".concat(auth.getName()).concat(" tienes acceso"));
        } else {
            logger.info("Hola ".concat(auth.getName()).concat(" NO tienes acceso"));
        }

        /**
         * Alternativa 3: usando el HttpServletRequest
         */
        if (request.isUserInRole("ROLE_ADMIN")){
            logger.info("Hola ".concat(auth.getName()).concat(" tienes acceso"));
        } else {
            logger.info("Hola ".concat(auth.getName()).concat(" NO tienes acceso"));
        }



        Pageable pageRequest = PageRequest.of(page, 8);
        Page<Cliente> clientes = clienteService.findAll(pageRequest);
        PageRender<Cliente> pageRender = new PageRender<>("/listar", clientes);

        model.addAttribute("titulo", messageSource.getMessage("text.cliente.listar.titulo", null, locale));
        model.addAttribute("clientes", clientes); //pasamos el listado de clientes
        model.addAttribute("page", pageRender);
        return "listar";
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(value = "/form")
    public String create(Map<String, Object> model, Locale locale){
        Cliente cliente = new Cliente();
        model.put("accion", messageSource.getMessage("text.cliente.form.titulo.crear", null, locale));
        model.put("cliente", cliente);
        model.put("titulo", messageSource.getMessage("text.cliente.form.titulo", null, locale));
        return "form";
    }

    @Secured("ROLE_USER")
    @GetMapping(value = "/ver/{id}")
    public String ver(@PathVariable(value = "id") Long id, Map<String, Object> model, RedirectAttributes flash, Locale locale){
        Cliente cliente = clienteService.fetchByIdWithFacturas(id); //clienteService.findOne(id);
        System.out.println(cliente.toString());
        System.out.println(cliente.getId());
        if (cliente == null){
            flash.addFlashAttribute("error", messageSource.getMessage("text.cliente.flash.db.error", null, locale));
            return "redirect:/listar";
        }
        model.put("cliente", cliente);
        model.put("titulo", messageSource.getMessage("text.cliente.detalle.titulo", null, locale).concat(": ").concat(cliente.toString()));
        model.put("tituloFacturas", messageSource.getMessage("text.cliente.factura.titulo.listado", null, locale).concat(": ").concat(cliente.toString()));
        return "ver";
    }


    /**
     * Este método representa otra manera de pasar una imagen a la vista, en la cual dicha imagen se carga de forma
     * programática a través de una respuesta usando Resource con UrlResource. Para usar este mecanizmo es necesario
     * comentar la clase MvcConfig ya que con este método, dicha clase no es necesaria (todo su contenido)
     * @param filename
     * @return
     */
    @Secured("ROLE_USER")
    @GetMapping(value = "/uploads/{filename:.+}") //El agregado :.+ es para que conserve la extensión del archivo como .jpg o .png
    public ResponseEntity<Resource> verFoto(@PathVariable String filename){
        Resource recurso = null;
        try {
            recurso = uploadFileService.load(filename);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename: \"" + recurso.getFilename() + "\"").body(recurso);
    }


    /**
     * Para poder aplicar las validaciones de la clase Entity es necesario marcar el parámetro com @Valid
     * Para poder mostrar en el form si algún parámetro ha fallado, tenemos que usar otro parámetro BindingResult
     *
     * El metodo clienteService.save(cliente) va a guardar un nuevo cliente en la bd, en el cual el ID siempre va en 0
     * ya que el mismo es un campo hidden y el valor es colocado automáticamente en el Entity. Por lo tanto, para editar
     * un campo ya existente debemos comprobar que el campo id del formulario sea = 0
     * @param cliente
     * @return
     */
    @Secured("ROLE_ADMIN")
    @RequestMapping(value = "/form", method = RequestMethod.POST)
    public String guardar(@Valid Cliente cliente, BindingResult result, Model model, @RequestParam(value = "file") MultipartFile foto, RedirectAttributes flash, SessionStatus status, Locale locale){
        if (result.hasErrors()){
            model.addAttribute("titulo", messageSource.getMessage("text.cliente.form.titulo", null, locale));
            return "form";
        }
        if (!foto.isEmpty()){

            //Con este if vamos a comprobar primero que el cliente exista y luego que ya tenga una foto, para poder luego reemplazarla correctamente
            if (cliente.getId() != null && cliente.getId() > 0 && cliente.getFoto() != null && cliente.getFoto().length() > 0){
                //Para borrar la foto primero debemos obtener la ruta absoluta de la imagen
                uploadFileService.delete(cliente.getFoto());
            }

            String uniqueFilename = null;
            try {
                uniqueFilename = uploadFileService.copy(foto);
            } catch (IOException e) {
                e.printStackTrace();
            }
            //String rootPath = "C://Temp//uploads";
            flash.addFlashAttribute("info", messageSource.getMessage("text.cliente.flash.foto.subir.success", null, locale) + "'" + uniqueFilename + "'");
            cliente.setFoto(uniqueFilename);
        }
        String mensajeFlash = (cliente.getId() != null)? messageSource.getMessage("text.cliente.flash.editar.success", null, locale) : messageSource.getMessage("text.cliente.flash.crear.success", null, locale);
        clienteService.save(cliente);
        status.setComplete();
        flash.addFlashAttribute("success", mensajeFlash); // con flash muestro mensajes
        return "redirect:/listar";
    }


    @Secured("ROLE_ADMIN")
    @RequestMapping(value = "/form/{id}")
    public String editar(@PathVariable(value = "id") Long id, Map<String, Object> model, RedirectAttributes flash, Locale locale){
        Cliente cliente = null;
        if (id > 0){
            cliente = clienteService.findOne(id);
            if (cliente == null) {
                flash.addFlashAttribute("error", messageSource.getMessage("text.cliente.flash.db.error", null, locale));
                return "redirect:/listar";
            }
        } else {
            flash.addFlashAttribute("error", messageSource.getMessage("text.cliente.flash.id.error", null, locale));
            return "redirect:/listar";
        }
        model.put("cliente", cliente);
        model.put("titulo", messageSource.getMessage("text.cliente.form.titulo", null, locale));
        model.put("accion", messageSource.getMessage("text.cliente.form.titulo.editar", null, locale));
        return "form";
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(value = "/eliminar/{id}")
    public String eliminar(@PathVariable(value = "id") Long id, RedirectAttributes flash, Locale locale){
        Cliente cliente = null;
        if (id > 0) {
            cliente = clienteService.findOne(id); //Para borrar la imagen primero tengo que obtener el cliente
            if (cliente == null){
                flash.addFlashAttribute("error", "El Id del cliente no existe en la BBDD");
                return "redirect:/listar";
            } else {
                clienteService.delete(id);
                flash.addFlashAttribute("success", messageSource.getMessage("text.cliente.flash.eliminar.success", null, locale));
                if (cliente.getFoto() != null && cliente.getFoto().length() > 0) {
                    if (uploadFileService.delete(cliente.getFoto())) {
                        String mensajeFotoEliminar = String.format(messageSource.getMessage("text.cliente.flash.foto.eliminar.success", null, locale), cliente.getFoto());
                        flash.addFlashAttribute("info", mensajeFotoEliminar);
                    }
                }
            }
            //Para borrar la foto primero debemos obtener la ruta absoluta de la imagen
        }
        return "redirect:/listar";
    }

    /**
     * Cualquier clase rol o que representa un rol en nuestra aplicación tiene que implementar GrantedAuthoriry. Con
     * este método podemos obtener un rol y de esa forma determinar si el usuario pertenece a algún rol y determinar
     * si el mismo tiene o no acceso a determinados recursos o no. Esta es una implementación en el controlador. En la
     * vista se hace mediante sec:authorize="hasRole('ROLE_ADMIN')"
     * @param role
     * @return
     */
    public boolean hasRole(String role){
        SecurityContext context = SecurityContextHolder.getContext(); //Se obtiene el contexto de forma estática

        if (context == null)
            return false;
        Authentication auth = context.getAuthentication();

        if (auth == null)
            return false;

        Collection<? extends GrantedAuthority> authorities = auth.getAuthorities();


        for (GrantedAuthority authority : authorities) {
            if (role.equals(authority.getAuthority())){
                logger.info("Hola usuario ".concat(auth.getName()).concat(" tu role es: ".concat(authority.getAuthority())));
                return true;
            }
        }
        return false;

        // return authorities.contains(new SimpleGrantedAuthority(role)); // esta es otra forma de verificar el rol
    }
}
