package com.bolsadeideas.springboot.app.models.service;

import com.bolsadeideas.springboot.app.models.entity.Cliente;
import com.bolsadeideas.springboot.app.models.entity.Factura;
import com.bolsadeideas.springboot.app.models.entity.Producto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface IClienteService {
    public List<Cliente> findAll(); //Devuelve todos los clientes
    public Page<Cliente> findAll(Pageable pageable); //importamos desde el springframework.data.domain
    public void save(Cliente cliente); //Se utiliza para guardar un nuevo cliente
    public Cliente findOne(Long id);
    public void delete(Long id);
    public List<Producto> findByNombre(String term);
    public void saveFactura(Factura factura);
    public Producto findProductoById(Long id);
    public Factura findFacturaById(Long id);
    public void deleteFactura(Long id);
    public Factura fetchFacturaByIdWithClienteWithItemFacturaWithProducto(Long id);
    public Cliente fetchByIdWithFacturas(Long id);
}
