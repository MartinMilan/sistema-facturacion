package com.bolsadeideas.springboot.app.models.service;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.MalformedURLException;

public interface IUploadFileService {

    public Resource load(String filename) throws MalformedURLException; //método para poder cargar la imagen

    public String copy(MultipartFile file) throws IOException; // Método para poder cambiar el nombre de la imagen y asegurarse de que sea único

    public boolean delete(String filename); //Método para saber si se eliminó o no

    public void deleteAll(); //Para borrar todo el directorio de forma recursiva con todas las imagenes
    public void init() throws IOException; //Para crear todo el directorio nuevamente
}
