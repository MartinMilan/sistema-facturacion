package com.bolsadeideas.springboot.app.models.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Esta es una clase Entity, lo que quiere decir que estará mapeada a una base de datos
 * @Id se usa para indicar que esa propiedad será la primary key
 * @GeneratedValue determina la estrategia con la cual se va a ir asignando el ID (IDENTITY signigica autoincremental +1)
 * @Column se usa para dar un nombre diferente a la columna correspondiente de la tabla de datos
 * @Temporal solo se usa para fechas, indica el formato en el que van a guardar las fechas
 *
 * Podemos usar reglas de validación en las distintas propiedades. Estas reglas son propias del estándar (javax.validation.constraints)
 * @NotEmpty: Básicamente me indica que el campo es requerido
 */
@Entity
@Table(name = "clientes")
public class Cliente implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotEmpty //Los strings deben usar @NotEmpty ya que @NotNull no hace efecto
    private String nombre;

    @NotEmpty
    private String apellido;

    @NotEmpty
    @Email
    private String email;

    @NotNull
    @Column(name = "create_at")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date createAt;

    /**
     * Con cascadeType.ALL indicamos que todas las operaciones de delete o persist se realizaran en casada
     * con el mappedBy asiganamos el atributo de la otra clase en la relación, asiendo la misma bidireccional
     *
     * orphanRemoval Sirve para remover registros huérfanos que no están asoiados a ningun cliente
     */
    @OneToMany(mappedBy = "cliente", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    //@JsonIgnore //Evitamos serializar esta propiedad y de esa forma evitamos el bucle infinito
    @JsonManagedReference
    private List<Factura> facturas;

    private String foto = "";

/*
    @PrePersist //Esta anotación indica que se debe ejecutar este método antes de realizar la persistencia
    public void prePersist(){
        createAt = new Date();
    }
*/

    public Cliente() {
        facturas = new ArrayList<Factura>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public List<Factura> getFacturas() {
        return facturas;
    }

    public void setFacturas(List<Factura> facturas) {
        this.facturas = facturas;
    }

    public void addFactura(Factura factura){
        facturas.add(factura);
    }

    @Override
    public String toString(){
        return nombre + " " + apellido;
    }
}
