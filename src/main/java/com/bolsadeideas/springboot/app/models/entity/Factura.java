package com.bolsadeideas.springboot.app.models.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "facturas")
public class Factura implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)//Autoincremental
    private Long id;

    @NotEmpty
    private String descripcion;

    private String observacion;

    @Temporal(TemporalType.DATE)
    @Column(name = "create_at")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date createAt;
    /**
     * Agregamos la anotacion @ManyToOne para indicar Muchas facturas (muchos this) un cliente (un atributo)
     * Además vamos a indicar carga perezosa
     *
     * EAGER: trae todo de una sola vez, es decir, si consultamos una factura también va a realizar una consulta al cliente
     * Si tuvieramos una lista de facturas, con este tipo de consulta, por cada factura también consultaría al cliente
     * correspondiente sobrecargando demasiado la base de datos
     *
     * con LAZY, las  consultas se realizan de forma perezosa, en este caso, cuando se realiza la consulta getCliente
     * ahi recien se realiza la del cliente
     */
    @ManyToOne(fetch = FetchType.LAZY) //Perezosa
    @JsonBackReference //evitamos serializar esta parte en el Json
    private Cliente cliente;

    /**
     * ItemsFactura no tiene una relación con Factura como la tiene Cliente, ya que no hay bidireccionalidad, en ningún
     * momento vamos a usar un ItemFactura para obtener la factura, pero si necesitamos para una factura determinada
     * obtener sus hijos, en este caso sus items, por lo tanto es muy importante tener los ids de cada item factura como
     * clave foranea, dicha clave se agrega con @JoinColumn(factura_id). Esto ultimo significa que en la tabla de items
     * vamos a tener el id de la factura correspondiente
     */
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "factura_id")
    private List<ItemFactura> items;


    public Factura() {
        items = new ArrayList<ItemFactura>();
    }

    public Long getId() {
        return id;
    }

    /**
     * Este método se encarga se crear la fecha de creación
     */
    @PrePersist
    public void prePersist(){
        createAt = new Date();
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    /**
     * Con este metodo evitamos bucle infinito al serializar a los clientes ya que cliente -> factura -> cliente
     * Entonces con esta notación no se ejecutará este método en la serialización
     * @return
     */
    @XmlTransient
    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public List<ItemFactura> getItems() {
        return items;
    }

    public void setItems(List<ItemFactura> items) {
        this.items = items;
    }

    public void addItemFactura(ItemFactura item){
        items.add(item);
    }
    /**
     * El siguiente método es para calcular en total de la factura
     */

    public Double getTotal(){
        Double total = 0.0;
        int size = items.size();
        for (int i = 0; i < size; i++) {
            total += items.get(i).calcularImporte();
        }
        return total;
    }
}
