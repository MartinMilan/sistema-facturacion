package com.bolsadeideas.springboot.app.models.service;

import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

@Service
public class UploadFileService implements IUploadFileService {

    private final static String UPLOADS_FOLDER = "uploads";

    @Override
    public Resource load(String filename) throws MalformedURLException {
        Path pathFoto = getPath(filename); //desde la raiz, del disco duro
        Resource recurso = null;
        recurso = new UrlResource(pathFoto.toUri());
        if (!recurso.exists() || !recurso.isReadable()) { //Si el recurso no existe y no es leible
            throw new RuntimeException("Error: no se puede cargar la imagen: " + pathFoto.toString());
        }

        return recurso;
    }

    @Override
    public String copy(MultipartFile file) throws IOException{

        String uniqueFilename = UUID.randomUUID().toString() + "_" + file.getOriginalFilename(); //De esta forma asignamos un nombre unico al archivo
        Path rootPath = getPath(uniqueFilename); //De esta manera se crea una ruta dentro del proyecto pero no estatica (relativa al proyecto)

        //foto.getOriginalFilename() representa el nombre del archivo, sin embargo puede repetirse, se podría cambiar para que este sea único
        /* byte[] bytes = foto.getBytes();
        Path rutaCompleta = Paths.get(rootPath + "//" + foto.getOriginalFilename());
        Files.write(rutaCompleta, bytes);*/


        Files.copy(file.getInputStream(), rootPath); //Podemos usar esta forma o la anterior, es lo mismo


        return uniqueFilename;
    }

    @Override
    public boolean delete(String filename) {
        Path rootPath = getPath(filename);
        File archivo = rootPath.toFile();

        if (archivo.exists() && archivo.canRead()){
            if (archivo.delete()){
                return true;
            }
        }
        return false;
    }

    @Override
    public void deleteAll() {
        FileSystemUtils.deleteRecursively(Paths.get(UPLOADS_FOLDER).toFile());
    }

    @Override
    public void init() throws IOException {
        Files.createDirectory(Paths.get(UPLOADS_FOLDER));
    }

    public Path getPath(String filename){
        return Paths.get(UPLOADS_FOLDER).resolve(filename).toAbsolutePath(); //desde la raiz, del disco duro. // De esta forma se agrega la ruta completa, la ruta absoluta
    }
}
