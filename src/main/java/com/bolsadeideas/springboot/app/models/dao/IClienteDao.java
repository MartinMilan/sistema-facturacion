package com.bolsadeideas.springboot.app.models.dao;

import com.bolsadeideas.springboot.app.models.entity.Cliente;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Acá se establece el contrato que debe tener los DAO para cliente  Clase de acceso a datos (Data Access Object)
 *
 * Vamos a convertir nuestra interfaz en una interfaz CRUD Repository
 *
 * Vamos a modificar para en ves de heredar de CrudRepository, heredemos de PagingAndSortingRepository
 * esta nueva interfaz nos permitirá poder incluir la paginación en nuestro proyecto para manejar de forma más
 * eficiente los datos.
 *
 * PagingAndSortingRepository hereda de CrudRepository pero agrega más funcionalidades (la laginación)
 */
public interface IClienteDao extends PagingAndSortingRepository<Cliente, Long> {

    @Query("select c from Cliente c left join fetch c.facturas f where c.id=?1")
    public Cliente fetchByIdWithFacturas(Long id);

}
